import datetime
import os
import sys
from typing import Iterable
from warnings import simplefilter

import datedelta

simplefilter("ignore")

S_NOT_PASSED = "不通过"
S_PASSED = "通过"
S_BYPASSED = "不稽核"

S_YES = "是"
S_NO = "否"

S_CSV_ENCODING = "gbk"


def get_year_month_by_delta(year, month, months=1):
    current_date = datetime.datetime(
        year=year, month=month, day=1
    )
    target_date = current_date + datedelta.MONTH * months
    return target_date.year, target_date.month


def _check_file(path):
    if not os.path.exists(path):
        raise FileNotFoundError(path)


def check_files(*args):
    for arg in args:
        if isinstance(arg, str):
            _check_file(arg)
        elif isinstance(arg, Iterable):
            for item in arg:
                _check_file(item)
        else:
            raise NotImplementedError(
                "Only string/Iterable[string] supported"
            )


def _get_date_arg():
    for arg in sys.argv:
        if arg.startswith("--date="):
            date_arg = arg[len("--date=") :]
            return int(date_arg[:2]), int(date_arg[2:])

    return None, None


def year_month():
    year, month = _get_date_arg()
    if not year:
        raw = input("请输入要稽核年 月，如21 4: ")
        year, month = [
            int(t) for t in raw.split(maxsplit=1)
        ]
    year += 2000
    year_new, month_new = get_year_month_by_delta(
        year, month
    )
    return (year, month), (year_new, month_new)


def last_n_month(year, month, n=3):
    return [
        get_year_month_by_delta(year, month, delta)
        for delta in range(-n, 1)
    ]


__all__ = [
    "S_NOT_PASSED",
    "S_PASSED",
    "S_BYPASSED",
    "S_YES",
    "S_NO",
    "S_CSV_ENCODING",
    "check_files",
    "get_year_month_by_delta",
    "year_month",
    "last_n_month",
]
