from os import path

import pandas as pd

from common import *

month = int(input("1~?月: "))

FP_GONGDAN_LIST = [
    f"data/0-工单/家客工单导出_潮州_2021-{month:02}.xlsx"
    for month in range(1, month + 1)
]


for f in FP_GONGDAN_LIST:
    print(f)
    df = pd.read_excel(f, dtype="string")
    basedir = path.dirname(f)
    filename, _ = path.splitext(path.basename(f))
    zip_options = dict(
        method="zip",
        archive_name=f"{filename}.csv",
    )
    df.to_csv(
        f"data/0-工单/{filename}.zip",
        compression=zip_options,
        encoding=S_CSV_ENCODING,
        index=False,
    )
