# %%
# 初始化
import pandas as pd

import reader
from common import *

(year, month), _ = year_month()

# %%
# 读取工单
usecols = (
    "工单号",
    "CRM工单号",
    "CRM业务流水号",
    "宽带帐号",
    "电视账号",
    "多媒体家庭电话",
    "操作类型",
    "是否施工标识",
    "接入方式",
    "产品名称",
    "资源类型",
    "派单日期",
    "结单时间",
    "工单状态",
    "地市",
    "派单人",
    "装维组/用户班",
    "装维人员/用户班人员",
    "客户类型",
    "五级地址ID",
    "综合网格",
    "处理渠道",
)
df = reader.read_gongdan(
    f"data/0-工单/家客工单导出_潮州_{year}-{month:02}.zip",
    usecols,
)

# %%
# 筛选数据
# 操作类型 = 业务拆除
# 是否施工 = 普通开通施工
# 资源类型 = 自建宽带
# 工单状态 = 归档
mask = (
    (df["操作类型"] == "业务拆除")
    & (df["是否施工标识"] == "普通开通施工")
    & (df["资源类型"] == "自建宽带")
    & (df["工单状态"] == "归档")
)
df = df[mask]

# %%
# 输出拆机单
df[["宽带帐号", "电视账号", "多媒体家庭电话", "归档时间"]].to_excel(
    f"report/1-拆机/{year}{month:02}_拆机单.xlsx",
    index=False,
)

# %%
# 拆机工单地市稽核明细表
df = df[df["产品名称"].isin(["手机宽带", "宽带", "手机宽带基础产品"])]

# %%
# 当月归档无效工单核查
df["当月归档无效工单核查"] = S_PASSED
df.loc[
    df["装维人员/用户班人员"].isin(["蔡启锐", "蔡荣华"]), "当月归档无效工单核查"
] = S_NOT_PASSED

# %%
# 质检核查
df_zhijian = pd.read_excel(
    f"data/1-拆机/{year}{month:02}_拆机归档人工质检不合格清单.xlsx",
    header=None,
    skiprows=1,
    dtype="string",
)
df["质检核查"] = S_PASSED
df.loc[
    df["宽带帐号"].isin(df_zhijian[0]), "质检核查"
] = S_NOT_PASSED


# %%
# 处理渠道核查
df["处理渠道核查"] = S_PASSED
df.loc[df["处理渠道"] != "手机客户端", "处理渠道核查"] = S_NOT_PASSED


# %%
# 超长工单核查
df["超长工单核查"] = S_PASSED
mask = df["归档时间"] - df["派单时间"] > pd.Timedelta(days=90)
df.loc[mask, "超长工单核查"] = S_NOT_PASSED

# %%
# 接入模式核查
df["接入模式核查"] = S_NOT_PASSED
df.loc[
    df["接入方式"].isin(["FTTH", "FTTB"]), "接入模式核查"
] = S_PASSED


# %%
# 地市稽核结果
CHECK_COLUMNS = [
    "当月归档无效工单核查",
    "质检核查",
    "处理渠道核查",
    "超长工单核查",
    "接入模式核查",
]

mask = (df[CHECK_COLUMNS] == S_PASSED).all(axis=1)
df["地市稽核结果"] = S_NOT_PASSED
df.loc[mask, "地市稽核结果"] = S_PASSED

# %%
# 不结算描述
def blocked_explain(x):
    reasons = []
    for col in CHECK_COLUMNS:
        if x[col] == S_NOT_PASSED:
            reasons.append(col)

    return ";".join(reasons)


df["不结算描述"] = df.apply(blocked_explain, axis=1)

# %%
# 输出：拆机工单地市稽核明细表
COLUMNS_DEL = [
    "电视账号",
    "多媒体家庭电话",
]

columns = [
    "工单号",
    "CRM工单号",
    "CRM业务流水号",
    "宽带帐号",
    "操作类型",
    "接入方式",
    "产品名称",
    "资源类型",
    "派单时间",
    "归档时间",
    "工单状态",
    "地市",
    "派单人",
    "装维组/用户班",
    "装维人员/用户班人员",
    "客户类型",
    "五级地址ID",
    "综合网格",
    "当月归档无效工单核查",
    "质检核查",
    "处理渠道核查",
    "超长工单核查",
    "接入模式核查",
    "地市稽核结果",
    "不结算描述",
]
df[columns].to_excel(
    f"report/1-拆机/{year}{month:02}_拆机工单地市稽核明细表.xlsx",
    index=False,
)
