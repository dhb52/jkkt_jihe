import glob
import subprocess

date = input("请输入要稽核年 月，如21 4: ")
scripts = sorted(glob.glob("*-*.py"))
for script in scripts:
    print(f"Running {script} ...")
    subprocess.run(["python", script, f"--date={date}"])
