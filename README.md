## 虚拟环境与依赖库安装

```cmd
python -m venv .venv
.venv\Script\activate
pip install -r requirements.txt
```

## 本项目依赖如下：

    openpyxl
    pandas

## 工具方面

> 使用 black 代码格式化工具，每行限制 60 个字符，在 `myproject.toml` 文件中配置
> 使用 isort 工具对 import 进行自动排序

建议修改代码后执行如下命令：

```
isort *.py
black *.py
```

## 数据

放在 data 目录，以如下文件命名方式存放

```
data/0-BI:
202109_BI宽带月报.zip

data/0-互感:
202109_互感数据.zip

data/0-工单:
家客工单导出_潮州_2021-09.zip

data/0-管线:
202109_管线数据.zip

data/1-拆机:
202109_拆机归档人工质检不合格清单.xlsx

data/2-宽带装移机:
202109_新装ONU管控报表清单.csv
202109_智能质检清单.xlsx
全市新增酒店类企宽明细白名单.xlsx

data/3-电视装移机:
202109_电视平台数据.zip
202109电视开通地市稽核明细报表.xlsx

data/4-IMS装移机:
202109_BI-IMS月报.xlsx
202109_IMS工单地市稽核明细报表.xlsx

data/5-智能组网:
202109_智能组网SDK测试清单.xlsx
202109_智能组网上线清单.xlsx

data/7-智能安防:
202109_智能安防人工质检不合格清单.xlsx
202109_智能安防绑定明细清单.xlsx

data/8-爱家TV:
202109_爱家TV-激活数据.zip
202109_爱家TV归档.zip
```

## 脚本

### 分业务

```
1-拆机.py
2-宽带装移机.py
3-电视装移机.py
4-IMS装移机.py
5-智能组网.py
6-提速.py
7-智能安防.py
8-爱家TV.py
```

### 汇总

```
runner.py           # 统一运行所有分类业务稽核
summary_V2.py       # 同装汇总
```

## 最终生成的数据报表

会放在`report`目录下

```
report/0-汇总:
202109_同装汇总表.xlsx

report/1-拆机:
202109_拆机单.xlsx
202109_拆机工单地市稽核明细表.xlsx

report/2-宽带装移机:
202109_宽带开通工单地市铁通核对表.xlsx
202109_宽带装移机工单地市稽核明细表.xlsx

report/3-电视装移机:
202109_电视开通稽核地市铁通核对.xlsx
202109_电视装移机地市稽核明细表.xlsx

report/4-IMS装移机:
202109_IMS开通稽核地市铁通核对.xlsx
202109_IMS装移机工单地市稽核明细表.xlsx

report/5-智能组网:
202109_智能组网工单地市稽核明细.xlsx

report/6-提速:
202109_提速工单稽核明细表.xlsx

report/7-智能安防:
202109_安防工单地市稽核结果.xlsx

report/8-爱家TV:
202109_爱家TV开通地市稽核明细.xlsx
```

## 注意事项

`互感数据.zip`等 zip 文件，其中的数据行给的有问题，多出了末尾的逗号(,)，导致列名与数据对应不上，这个内容在 reader.py 专门处理了这个问题。

## 相关课程

[中移网大《pandas 数据处理实战》](https://wangda.chinamobile.com/#/study/course/detail/47c9ba3d-876b-4118-b0ca-0c5df8e09716)
